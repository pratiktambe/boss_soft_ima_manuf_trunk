#!/bin/bash
# Pollock factory testing / installation script

cd /root/factory

## make our screen clean
echo 0 >/proc/sys/kernel/printk
TERM=linux

ANSWER=.answer_r220
ANSWER_DELL_HARDWARE=.answer_aparato_hardware_r220

DIALOG='dialog --colors'

REPORT=report_r220.txt

report_error()
{
#  echo "To: Production <production@list.brivo.com>" > $REPORT
  echo "To: Production <mfgerrors@brivo.com>" > $REPORT
#  echo "From: Pollock <factory@pollock.brivo.net>" >> $REPORT
  echo "From: Bethesda Pollock <factory@pollock.brivo.com>" >> $REPORT
  echo "Subject: Pollock error:" $1 >> $REPORT
  echo "" >> $REPORT
  echo "server.out:"  >> $REPORT
  cat server.out >> $REPORT
  echo "response_r220:" >> $REPORT
  cat response_r220 >> $REPORT
  /usr/sbin/sendmail -r factory@pollock.brivo.net mfgerrors@brivo.com < $REPORT
}

reset_pcan_hub()
{
  ipdc/reset_pcan.php 
  sleep 2
  ipdc/reset_pcan.php 
  sleep 2
  ifup $FOR_ACS5000_ETH >/dev/null 2>&1
  ifup ${FOR_ACS5000_ETH}:0 >/dev/null 2>&1
}

mac_formater()
{
    local mac=`echo $1 | tr '[:lower:]' '[:upper:]'`
    local mymac=""
    local i=0

    echo $str |grep ':' -q
    [ $? -eq 0 ] && echo $mac && return
    
    for i in `seq 0 2 10`; do
      if [ $i -eq 0 ]; then
        mymac=${mac:$i:2}
      else
        mymac="${mymac}:${mac:$i:2}"
      fi
    done
    echo $mymac
}

#
# put log in syslog facility
#
LOGGER()
{
    logger -t "pollock.sh [$$]" $*
}

#
# Check main network link. 0->OK, 1->fail
#
check_main_network_link()
{
    ## check physical link
    mii-tool eth0 | grep -q 'link ok'

    [ $? -ne 0 ] && return 1

    return 0
}

check_main_network_link1()
{
    ## check physical link
    ethtool eth0 | grep -q 'Link detected: yes'
    
    [ $? -ne 0 ] && return 1

    return 0
}

#
# Check type of bootloader
#
check_bootloader()
{
    ## check physical link
    mii-tool $FOR_ACS5000_ETH | grep -q 'link ok'
    if [ $? -ne 0 ]; then
        ## Warning people
        $DIALOG --title "\Z1ERROR" --colors --msgbox \
            "\Z1Please check ethernet cable if plugged into Pollock and ACS5000 board proberly and powered board." \
            10 70
        return 2
    fi
    ## due to ethloader is very subtle. Make sure we only turn on those service when we need.
    #/etc/init.d/nfs stop >/dev/null 2>&1
    #killall in.tftpd >/dev/null 2>&1
    #etc/init.d/xinetd stop >/dev/null 2>&1

    $DIALOG --infobox "Checking bootloader type. (May take 30 seconds..)" 10 70
    ./ethloader $BOARD_ADDR 86:95:0B:12:34:BC -e=1,2 2>&1 1>> ethloader.log
    [ $? -ne 0 ] && return 1

    return 0
}

#
# Make sure main network is working
#
sanity_check()
{
    if check_main_network_link ; then
        ## run cisco vpn                            
        if [ -e /etc/vpnc.conf ]; then
          if [ ! -e /var/run/vpnc/pid ]; then
            # not running. launch it
            [ -x /usr/sbin/vpnc ] && /usr/sbin/vpnc >/dev/null 2>&1
          fi
        fi
    else
        ## Warning people
        $DIALOG --title "\Z1ERROR" --colors --msgbox \
            "\Z1Failed to communicate with server, aborting.\nPlease check ethernet cable if plugged properly." \
            10 70
        exec $0
    fi
}

# Serial number check
# Make sure the serial number is in proper format
check_serial_number()
{
    local sn=$1

    [ "$SERIAL_FORMAT_CHECK" = "" ] && echo $sn && return
    
    echo "$sn" | grep "$SERIAL_FORMAT_CHECK" >/dev/null 2>&1
    if [ $? -eq 0 ]; then
       echo $sn
    else
       [ "$LOG_WRONG_SERIAL" = '1' ] && LOGGER "wrong serial number format: '$sn'"
        echo "";
    fi
}

# Prompt the user for a serial number
# (Should always be scanned by bar code reader)
read_serial_number()
{
  local check_format=$2
  serial_number=
  cpu_serial_number=
  until [ ! -z "$serial_number" ]
  do
    $DIALOG --title "$1 Board Serial Number" \
    --inputbox "Scan barcoded serial number from board (Press ESC to return to Main Menu.)" \
    8 75 2> $ANSWER
    if [ $? -ne 0 ]
    then
      return
    fi
    serial_number=`cat $ANSWER`
    if [ "$FORCE_SERIAL_UPPER_CASE" = '1' ]; then
        serial_number=`echo $serial_number |tr '[:lower:]' '[:upper:]'`
    fi
    [ "$check_format" = "1" ] && serial_number=`check_serial_number "$serial_number"`
  done
}

installmain()
{
  while true ; do
    sanity_check
    # Board type
          $DIALOG --title "Main Board Type" \
        --menu "Select a board type by number and press ENTER" 15 55 8 \
        "aparato" "OnSite Aparato appliance" \
        2> $ANSWER
    
    if [ $? -ne 0 ]; then
      return
    fi
    do_old=0
    board_type=`cat $ANSWER`
    ## todo: sanity check
     case $board_type in
       "aparato") 
         model=APARATO
         print_model=APARATO
         FW_VER=$APARATO_VER
         LABLE_VER=$APARATO_VER
         STANDALONE_HW="onsite-aparato"
         ;;
     esac

     ifdown $FOR_ACS5000_ETH >/dev/null 2>&1
     ifup $FOR_ACS5000_ETH >/dev/null 2>&1

     ifdown ${FOR_ACS5000_ETH}:0 >/dev/null 2>&1
     ifup ${FOR_ACS5000_ETH}:0 >/dev/null 2>&1
     ifconfig $IPDC_ETH down  >/dev/null 2>&1

     read_serial_number "Main" 1
     if [ -z "$serial_number" ]
     then
       return
     fi

     case $board_type in
       "aparato")
           $DIALOG --title "Select Aparato hardware" \
           --menu "Select a Aparato hardware Type by number and press ENTER" 15 55 5 \
                   "NON_TPM" "Software IMA (e.g. Dell R220) - Released circa 2014 (Default)" \
                    2> $ANSWER_DELL_HARDWARE

            aparato_hardware_type=`cat $ANSWER_DELL_HARDWARE`

            appliance_ip=169.254.242.42
            $DIALOG --title "Connect Machine" --msgbox \
                "Set up machine for installation:\n  1. Plug Installation USB Flash drive into USB socket\n  2. Connect monitor, keyboard, AC power and Ethernet(Gb1)\n  3. Power machine on\n  4. After system boot, Press \Z4F11\Z0 for BIOS boot Manager\n  5. Choose boot from usb stick and make it boot.\n\nPress ENTER to continue." \
                20 70
            # aparato appliances are full-blown PCs that boot from USB to an empty
            # environment that has sshd waiting for a keyed login.
            $DIALOG --infobox "Retrieving information from Brivo Key server.." 10 70
            rm -f response_r220
            curl -k -G --fail \
                -d type=main_start \
                -d serial="$serial_number" \
                -d expver="$EXP_VER" \
                -d mainver="$APARATO_VER" \
                -d hwrev="1" \
                -d hwtype="1" \
                -o response_r220 \
                https://$KEY_SERVER/$KEY_SERVER_URL >& server.out

            if [ $? -ne 0 ]; then
                $DIALOG --title "\Z1ERROR" --colors --msgbox \
                    "\Z1Failed to communicate with server, aborting.\nPlease retry operation before contacting Brivo for assistance." \
                    10 70
                report_error "Failed to post aparato main_start:"
                return
            fi
            booted=0
            # Wait up to 100 seconds for system to boot
            for a in `seq 0 1 100`; do
                echo $a | $DIALOG --gauge "Waiting for system to boot.." 10 70
                ping -c 1 $appliance_ip >& /dev/null
                if [ $? -eq 0 ] ; then
                    booted=1
                    break
                fi
                sleep 1
            done
            if [ $booted -eq 0 ] ; then
                $DIALOG --title "\Z1ERROR" --colors --msgbox \
                    "\Z1Failed to communicate with appliance, aborting.\nPlease verify connections and retry operation before contacting Brivo for assistance." \
                    10 70
                return
            fi

            if [ "$aparato_hardware_type" = "NON_TPM" ]; then
                    scp install-aparato220-non-tpm aparato/aparato-raw-${APARATO_VER}.tgz response_r220 rebootme $appliance_ip:/tmp >/dev/null 2>&1
            fi
            if [ "$APARATO_PATCH_FILE" != "" ]; then
                if [ -e aparato/${APARATO_PATCH_FILE} ]; then
                    LABLE_VER=${APARATO_VER}-${PATCH_VER}
                    scp aparato/${APARATO_PATCH_FILE} $appliance_ip:/tmp >/dev/null 2>&1
                else
                    $DIALOG --title "\Z1ERROR" --colors --msgbox \
                        "\Z1Unable to retrieve patch file:$APARATO_PATCH_FILE, aborting.\nPlease contact Brivo for assistance." \
                        10 70
                    return
                fi
            fi

            if [ "$aparato_hardware_type" = "NON_TPM" ]; then
                    ssh root@$appliance_ip bash /tmp/install-aparato220-non-tpm ${APARATO_VER} "${APARATO_PATCH_FILE}" | \
            tee aparato-install-r220.out | $DIALOG --gauge "Installing firmware on appliance.." 10 70
            fi
            grep PASS aparato-install-r220.out
            if [ $? -ne 0 ] ; then
                $DIALOG --title "\Z1ERROR" --msgbox \
                    "\Z1Error installing software on appliance.  Please CHECK ALL CABLES and retry operation before contacting Brivo for assistance.\n\nIf it stop at 3%, please make sure you have updated the BIOS.\nIf it stop at 4%, please replace CMOS battery." 10 70
                return
            fi

            $DIALOG --title "Software Installed" --msgbox \
                "Software is installed.  Machine will now be rebooted for testing.  Please press ENTER to continue." \
                20 70

            for a in `seq 0 1 5`; do
                echo $a | $DIALOG --gauge "Waiting for system to reboot.." 10 70
                sleep 1
            done

            booted=0
            # Wait up to 100 seconds for system to boot
            for a in `seq 0 1 100`; do
                echo $a | $DIALOG --gauge "Waiting for system to boot.." 10 70
                # Preconfigured IP address
                ping -c 1 169.254.242.207 >& /dev/null
                if [ $? -eq 0 ] ; then
                    booted=1
                    break
                fi
                sleep 1
            done

            # Time for server to come up
            sleep 15

            if [ $booted -eq 0 ] ; then
                $DIALOG --title "\Z1ERROR" --colors --msgbox \
                "\Z1Failed to communicate with appliance while waiting for it to boot, aborting.\nPlease verify connections and retry operation before contacting Brivo for assistance." \
                10 70
                return
            fi

            # Check via pre-configured IP address
            rm -f tpmcheck
            curl --fail http://169.254.242.207/cgi-bin/acs.fcgi -o tpmcheck >& /dev/null
            if [ $? -ne 0 ] ; then
                $DIALOG --title "\Z1ERROR" --colors --msgbox \
                    "\Z1Failed to communicate with appliance webserver after booting, aborting.\nPlease verify connections and retry operation before contacting Brivo for assistance." \
                10 70
                return
            fi
            grep "SYSTEM STARTUP ERROR" -q tpmcheck

            if [ $? -eq 0 ] ; then
                $DIALOG --title "\Z1ERROR" --colors --msgbox \
                    "\Z1Failed. TPM check error.\nPlease contact Brivo for assistance." \
                10 70
                return
            fi
            curl -k -G --fail \
                -d type=main_finish \
                -d serial="$serial_number" \
                -o response_r220 \
                https://$KEY_SERVER/$KEY_SERVER_URL >& server.out

            if [ $? -ne 0 ]; then
                $DIALOG --title "\Z1ERROR" --msgbox \
                    "\Z1Failed to communicate with server, aborting.\nPlease retry operation before contacting Brivo for assistance." \
                    10 70
                    report_error "Failed posting aparato main_finish"
                return
            fi

            # OID is needed for the label
            oid=`cat response_r220  | awk '{print $2}'`

            # Print 1 mini sticker for for unit, 1 for box
            sed -e "s/_CPID_/$oid/g" -e "s/ACS5000-_MODEL_/OnSite Aparato/g" -e "s/_SERIAL_/$serial_number/g" < small-label-template.zpl > $SMALL_PRINTER

            cat logo.grf > $PRINTER

            sed -e "s/\^FDSE\^FS/\^FDAP\^FS/" -e "s/OnSite SE/OnSite Aparato/" -e "s/_CPID_/$oid/g" -e "s/_SERIAL_/$serial_number/g" -e "s/_VERSION_/$LABLE_VER/g" < se-identity-label-template.zpl > $PRINTER
            $DIALOG --title "Finished" --msgbox \
               "Operation complete.\nPlease place one label on unit, and place second label on outside of box." \
               20 70
        ;;
     esac
   done
}

mainmenu()
{
  . boss_r220.conf
    ifup $FOR_ACS5000_ETH >/dev/null 2>&1
    ifup ${FOR_ACS5000_ETH}:0 >/dev/null 2>&1

	$DIALOG --nocancel --title "Main Menu" \
    --menu "Select an option by number and press ENTER" 15 55 10 \
    "1" "Manufacture BOSS R220 APARATO" \
    "Q" "Quit" \
		2> $ANSWER
		
	case `cat $ANSWER` in
		"1") installmain 
      ;;
		"Q") exit 0 
      ;;
		*) 
      ;;
	esac
}

while true ; do
  sanity_check
  mainmenu
done

